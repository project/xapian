<?php

/**
 * @file
 * Installation file for xapian search library support to search_api module.
 */

define('SEARCH_API_XAPIAN_MINIMUM_BINDINGS', '1.0.2');

/**
 * Implementation of hook_requirements().
 *
 * Report Xapian bindings version if available, otherwise display
 * include error.
 */
function search_api_xapian_requirements($phase) {
  if (module_exists('xapian')) {
    return array();
  }
  $t = get_t();
  if (search_api_xapian_is_xapian_available()) {
    // Get Xapian bindings version, supporting PHP4 and PHP5 methods.
    $version = search_api_xapian_get_xapian_version();
    if (version_compare($version, SEARCH_API_XAPIAN_MINIMUM_BINDINGS) < 0) {
      $severity = REQUIREMENT_ERROR;
      $version = $t('Your Xapian bindings are too old (%version).  You must install at least version %minimum.', array('%version' => $version, '%minimum' => SEARCH_API_XAPIAN_MINIMUM_BINDINGS));
    }
    else {
      $severity = REQUIREMENT_OK;
    }
  }
  else {
    $severity = REQUIREMENT_ERROR;
    $version = $t('You need to install Xapian bindings version >= %minimum. <em>%error</em>', array('%minimum' => SEARCH_API_XAPIAN_MINIMUM_BINDINGS, '%error' => $GLOBALS['search_api_xapian_include_error']));
  }

  return array(
    'xapian' => array(
      'title' => $t('Xapian bindings'),
      'value' => $version,
      'severity' => $severity,
    ),
  );
}

/**
 * Provide default error handler for xapian.
 *
 * @param int $errno
 * @param string $errstr
 * @param string $errfile
 * @param int $errline
 * @param unknown_type $errcontext
 */
function search_api_xapian_requirements_error_handler($errno, $errstr, $errfile = NULL, $errline = NULL, $errcontext = NULL) {
  $GLOBALS['search_api_xapian_include_error'] = $errstr;
}

/**
 * Attempt to include xapian.php.  If there are errors, Xapian is not
 * available, otherwise it is available.
 *
 * @return bool
 */
function search_api_xapian_is_xapian_available() {
  $available = &drupal_static(__FUNCTION__, FALSE);

  if ($available === FALSE) {
    $GLOBALS['search_api_xapian_include_error'] = NULL;
    set_error_handler('search_api_xapian_requirements_error_handler');
    include_once 'xapian.php';
    restore_error_handler();

    if (NULL == $GLOBALS['search_api_xapian_include_error']) {
      $available = TRUE;
    }
  }

  return $available;
}

/**
 * Get the version string of Xapian.
 */
function search_api_xapian_get_xapian_version() {
  return (function_exists('xapian_version_string')) ? xapian_version_string() : Xapian::version_string();
}

/**
 * Implements hook_uninstall().
 */
function search_api_xapian_uninstall() {
  if (module_exists('search_api')) {
    // Delete servers associated with our implementation.
    \Drupal::database()->delete('search_api_server')
      ->condition('class', 'search_api_xapian_local_service')
      ->execute();
    \Drupal::database()->delete('search_api_server')
      ->condition('class', 'search_api_xapian_server_service')
      ->execute();
  }
}
